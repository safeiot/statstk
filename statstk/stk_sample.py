#!/usr/bin/env python3
import json
from argparse import ArgumentParser, FileType
import sys
from numpy import array
from hashlib import md5


def sample(data,
           hash_func=lambda x: md5(x.encode()).hexdigest(),
           hash_func_name='md5'):
    lines = data.split("\n")
    samples = list(float(l.strip()) for l in lines if l.strip() != "")
    samples = array(samples)
    return {
        'hash_digest': hash_func(data),
        'hash_func': hash_func_name,
        'size': len(samples),
        'mean': samples.mean(),
        'std': samples.std(),
        'max': samples.max(),
        'min': samples.min(),
    }


def main():
    parser = ArgumentParser(description='Compute stats of a sample.')
    parser.add_argument('i',
                        metavar='INPUT',
                        help="The samples should be a sequence of numbers (a number per line). Use \'-\' for stdin.",
                        type=FileType('rt'))
    parser.add_argument('-o',
                        metavar='OUTPUT',
                        type=FileType('w'),
                        default=sys.stdout,
                        help='A JSON file with stats about the samples. Default: stdout', )
    results = parser.parse_args()
    data = results.i.read()
    if len(data.strip()) == 0:
        sys.stderr.write("stk-stats: ERROR no input provided.\n")
        sys.exit(-1)
    json.dump(sample(data), results.o)


if __name__ == '__main__':
    main()
