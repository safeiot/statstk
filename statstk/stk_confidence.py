#!/usr/bin/env python3
from argparse import ArgumentParser, FileType, ArgumentTypeError
from scipy import stats, array
import math
import json

CONFIDENCE_COEFICIENT = .95


def critical_value(sample_size: int, confidence_coeficient: float, large_sample: int = 30):
    """
    Computes the critical value given a NumPy array of samples
    and a confidence coeficient (defaults to 0.95).
    """
    alpha = 1.0 - confidence_coeficient
    if sample_size >= large_sample:
        # Z_{1 - alpha/2}:
        return stats.norm.ppf(1 - alpha / 2)
    # degrees of freedom
    df = sample_size - 1
    # t_{1 - alpha/2}:
    return stats.t.ppf(1 - alpha / 2, df)


def margin_error(sample_size: int, sample_std, confidence_coeficient: float):
    cval = critical_value(sample_size, confidence_coeficient)
    return cval * sample_std / math.sqrt(sample_size)


def parse_coeficient(value):
    value = float(value)
    if value < 0.0 or value > 1.0:
        msg = "%r is not between 0 and 1" % value
        raise ArgumentTypeError(msg)
    return value


def main():
    import sys
    parser = ArgumentParser(description='Compute the margin error for a given confidence coeficient.')
    parser.add_argument('fdin',
                        metavar='INPUT',
                        type=FileType('r+'),
                        help='A JSON file with stats about the samples. Use \'-\' for stdin.', )
    group = parser.add_mutually_exclusive_group()
    group.add_argument('-o',
                       dest='fdout',
                       metavar='OUTPUT',
                       type=FileType('w'),
                       default=sys.stdout,
                       help='A JSON file with stats about the samples. Default: stdout.')
    group.add_argument('-i', action="store_true", default=False, dest="inplace", help='Writes file in place.')

    parser.add_argument('-c', metavar='coeficient', type=parse_coeficient, default=.95,
                        help="The confidence coeficient (between 0 and 1).")
    results = parser.parse_args()
    fdin = results.fdin
    if results.inplace:
        if fdin == sys.stdin:
            fdout = sys.stdout
        else:
            fdout = results.fdin
    else:
        fdout = results.fdout
    try:
        data = json.load(fdin)
        # In case we are editing a file, reset the file pointer
        if results.inplace and fdin is not sys.stdin:
            fdout.seek(0)  # reset the tape

        merr = margin_error(data['size'], data['std'], results.c)
        data['margin_error'] = merr
        data['confidence_coeficient'] = results.c
        json.dump([data], fdout)
    finally:
        fdin.close()
        fdout.close()
        # fdout does not need to be closed


if __name__ == '__main__':
    main()
