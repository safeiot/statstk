#!/usr/bin/env python3
from argparse import ArgumentParser, FileType, ArgumentTypeError
from scipy import stats, array
import math
import json


def diff_mean(data1, data2) -> float:
    return data1['mean'] - data2['mean']


def diff_std(data1, data2) -> float:
    x = (data1['std'] ** 2) / data1['size']
    y = (data2['std'] ** 2) / data2['size']
    return math.sqrt(x + y)


def diff_critical_value(data1, data2, confidence_coeficient, large_sample=30) -> float:
    alpha = 1.0 - confidence_coeficient
    if min(data1['size'], data2['size']) >= large_sample:
        # Z_{1 - alpha/2}:
        return stats.norm.ppf(1 - alpha / 2)
    else:
        # degrees of freedom
        x = (data1['std'] ** 2) / data1['size']
        y = (data2['std'] ** 2) / data2['size']
        xx = x ** 2 / (data1['size'] - 1)
        yy = y ** 2 / (data2['size'] - 1)
        df = (x + y) ** 2 / (xx + yy)
        df = int(round(df))
        # t_{1 - alpha/2}:
        return stats.t.ppf(1 - alpha / 2, df)


def diff_margin_error(data1, data2, confidence_coeficient) -> float:
    cval = diff_critical_value(data1, data2, confidence_coeficient)
    return cval * diff_std(data1, data2)


def parse_coeficient(value) -> float:
    value = float(value)
    if value < 0.0 or value > 1.0:
        msg = "%r is not between 0 and 1" % value
        raise ArgumentTypeError(msg)
    return value


def compare(data1, data2, coeficient) -> dict:
    mean = diff_mean(data1, data2)
    merr = diff_margin_error(data1, data2, coeficient)
    return {
        'mean': mean,
        'std': diff_std(data1, data2),
        'confidence_coeficient': coeficient,
        'margin_error': merr,
        'comparable': not ((mean - merr) <= 0 and 0 <= (mean + merr)),
        'ratio': (mean / data1['mean']),
    }


def main() -> None:
    import sys
    parser = ArgumentParser(description='Compares two means.')
    parser.add_argument('fdin1',
                        metavar='INPUT1',
                        type=FileType('r'),
                        help='A JSON file with stats about the samples. Use \'-\' for stdin.', )
    parser.add_argument('fdin2',
                        metavar='INPUT2',
                        type=FileType('r'),
                        help='A JSON file with stats about the samples. Use \'-\' for stdin.', )
    parser.add_argument('-o',
                        dest='fdout',
                        metavar='OUTPUT',
                        type=FileType('w'),
                        default=sys.stdout,
                        help='A JSON file with stats about the samples. Default: stdout.')
    parser.add_argument('-c', metavar='coeficient', type=parse_coeficient, default=.95,
                        help="The confidence coeficient (between 0 and 1).")
    results = parser.parse_args()
    fdin1 = results.fdin1
    fdin2 = results.fdin2
    fdout = results.fdout
    try:
        data1 = json.load(fdin1)
        data2 = json.load(fdin2)
        output = compare(data1, data2, results.c)
        json.dump(output, fdout)
    finally:
        fdin1.close()
        fdin2.close()
        fdout.close()
        # fdout does not need to be closed


if __name__ == '__main__':
    main()
